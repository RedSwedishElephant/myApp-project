import { Injectable } from '@angular/core';
import { Http } from '@angular/http'

@Injectable()
export class DataService {

  constructor(private http: Http ) { }

  fetchData(){
    return this.http.get("http://www.mocky.io/v2/5a21756b2d00006127e00328").subscribe(
      (data) => console.log(data)
    );
  }

}
