import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


import { AppComponent } from './app.component';
import { ArticoloComponent } from './articolo/articolo.component';
import { SecondoComp } from './secondo-component/secondo-comp.componet';

@NgModule({
  declarations: [
    AppComponent,
    SecondoComp
    
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
